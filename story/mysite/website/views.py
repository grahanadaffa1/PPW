from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse

def index(request):
    return render(request, 'website/prototypehtml.html')

def gallery(request):
    return render(request, 'website/gallery.html')

def blog(request):
    return render(request, 'website/blog.html')

def guestBook(request):
    return render(request, 'website/guestBook.html')

def resume(request):
    return render(request, 'website/resume.html')
